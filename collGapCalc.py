import sys, os
import numpy as np
import pandas as pd
import csv

nrj1=450e9
nrj=7000e9
exn=2.5e-6
pmass=0.9382720813e9


b1Colls=['TCTPH.6L1.B1','TCTPV.6L1.B1','TCTPXH.4L1.B1','TCTPXV.4L1.B1','TCTPH.6L5.B1','TCTPV.6L5.B1','TCTPXH.4L5.B1','TCTPXV.4L5.B1','TCDQA.A4R6.B1','TCDQA.C4R6.B1','TCDQA.B4R6.B1','TCSP.A4R6.B1','TCLPX.4R1.B1','TCL.5R1.B1','TCL.6R1.B1','TCLPX.4R5.B1','TCL.5R5.B1','TCL.6R5.B1']
b2Colls=['TCTPXV.4R1.B2','TCTPXH.4R1.B2','TCTPV.6R1.B2','TCTPH.6R1.B2','TCTPXV.4R5.B2','TCTPXH.4R5.B2','TCTPV.6R5.B2','TCTPH.6R5.B2','TCDQA.B4L6.B2','TCDQA.C4L6.B2','TCDQA.A4L6.B2','TCSP.A4L6.B2','TCLPX.4L1.B2','TCL.5L1.B2','TCL.6L1.B2','TCLPX.4L5.B2','TCL.5L5.B2','TCL.6L5.B2']
sigInj=[15.4,15.4,15.4,15.4,15.4,15.4,15.4,15.4,9.5,9.5,9.5,8.3,0,0,0,0,0,0] # TCLs at parking
sig150=[11.4,11.4,11.4,11.4,11.4,11.4,11.4,11.4,11.1,11.1,11.1,11.1,14.2,14.2,14.2,14.2,14.2,14.2]
b1Plane=['H','V','H','V','H','V','H','V','H','H','H','H','H','H','H','H','H','H']
b2Plane=['V','H','V','H','V','H','V','H','H','H','H','H','H','H','H','H','H','H']

injFile='450GeV_inj_6000.twiss'
ftNoXingFile='7000GeV_FT_noXing_1000.twiss'
ftXingFile='7000GeV_FT_xing_1000.twiss'
coll640File='7000GeV_xing_640.twiss'
coll200File='7000GeV_xing_200.twiss'
coll150File='7000GeV_xing_150.twiss'

def twissImporter(path):
    try:
        tempTwiss=np.genfromtxt(path, dtype=None, encoding=None, delimiter=',')
    except:
        print(' unable to open file %s - aborting...' % ( path ))
        sys.exit()
    startRowPos=np.flatnonzero(np.core.defchararray.find(tempTwiss,'NAME')!=-1)
    tempTwiss=tempTwiss[startRowPos[1]:]
    tempTwiss=list(map(lambda x: x.split(None,-1),tempTwiss))
    if( tempTwiss[0][0] != 'NAME' ):
        tempTwiss[0] = tempTwiss[0][1:]
    rows = [item[0][1:-1] for item in tempTwiss[2:]]
    cols = tempTwiss[0]
    tempTwiss = pd.DataFrame(tempTwiss[2:], index=rows, columns=cols)
    for col in cols[1:]:
        tempTwiss[col]=pd.to_numeric(tempTwiss[col],downcast='float')
    return(tempTwiss)

injTwiss = twissImporter('out/LHC_b1_'+injFile)
ftNoXingTwiss = twissImporter('out/LHC_b1_'+ftNoXingFile)
ftXingTwiss = twissImporter('out/LHC_b1_'+ftXingFile)
coll640Twiss = twissImporter('out/LHC_b1_'+coll640File)
coll200Twiss = twissImporter('out/LHC_b1_'+coll200File)
coll150Twiss = twissImporter('out/LHC_b1_'+coll150File)

header=['collimator','plane',
'inj beta','FT noXing beta','FT xing beta','640 beta','200 beta','150 beta',
'inj sig','FT noXing sig','FT xing sig','640 sig','200 sig','150 sig',
'inj gap','FT noXing gap','FT xing gap','640 gap','200 gap','150 gap']
b1Res=[header]
for i,collim in enumerate(b1Colls):
    if( i < 8 or i > 11):
        tct=True
    else:
        tct=False
    if( b1Plane[i]=='H' ):
        betCol='BETX'
    else:
        betCol='BETY'

    beta1 = injTwiss.loc[collim][betCol]                    
    beta2 = ftNoXingTwiss.loc[collim][betCol]               
    beta3 = ftXingTwiss.loc[collim][betCol]                 
    beta4 = coll640Twiss.loc[collim][betCol]                
    beta5 = coll200Twiss.loc[collim][betCol]                
    beta6 = coll150Twiss.loc[collim][betCol]
    sig1 = sigInj[i]
    gap1 = sig1*np.sqrt(beta1*exn*pmass/nrj1)*1e3
    sig6 = sig150[i]
    gap6 = sig6*np.sqrt(beta6*exn*pmass/nrj)*1e3

    if( tct ):
        sig2 = gap6/(np.sqrt(beta2*exn*pmass/nrj)*1e3)
        sig3 = gap6/(np.sqrt(beta3*exn*pmass/nrj)*1e3)
        sig4 = gap6/(np.sqrt(beta4*exn*pmass/nrj)*1e3)
        sig5 = gap6/(np.sqrt(beta5*exn*pmass/nrj)*1e3)
        gap2 = gap6
        gap3 = gap6
        gap4 = gap6
        gap5 = gap6
    else:
        sig2 = sig6
        sig3 = sig6
        sig4 = sig6
        sig5 = sig6
        gap2 = sig2*np.sqrt(beta2*exn*pmass/nrj)*1e3
        gap3 = sig3*np.sqrt(beta3*exn*pmass/nrj)*1e3
        gap4 = sig4*np.sqrt(beta4*exn*pmass/nrj)*1e3
        gap5 = sig5*np.sqrt(beta5*exn*pmass/nrj)*1e3
    tempRow = [collim,b1Plane[i],
                beta1,beta2,beta3,beta4,beta5,beta6,
                sig1,sig2,sig3,sig4,sig5,sig6,
                gap1,gap2,gap3,gap4,gap5,gap6]       
    b1Res.append(tempRow) 

injTwiss = twissImporter('out/LHC_b2_'+injFile)
ftNoXingTwiss = twissImporter('out/LHC_b2_'+ftNoXingFile)
ftXingTwiss = twissImporter('out/LHC_b2_'+ftXingFile)
coll640Twiss = twissImporter('out/LHC_b2_'+coll640File)
coll200Twiss = twissImporter('out/LHC_b2_'+coll200File)
coll150Twiss = twissImporter('out/LHC_b2_'+coll150File)

b2Res=[header]
for i,collim in enumerate(b2Colls):
    if( i < 8 or i > 11):
        tct=True
    else:
        tct=False
    if( b2Plane[i]=='H' ):
        betCol='BETX'
    else:
        betCol='BETY'

    beta1 = injTwiss.loc[collim][betCol]                    
    beta2 = ftNoXingTwiss.loc[collim][betCol]               
    beta3 = ftXingTwiss.loc[collim][betCol]                 
    beta4 = coll640Twiss.loc[collim][betCol]                
    beta5 = coll200Twiss.loc[collim][betCol]                
    beta6 = coll150Twiss.loc[collim][betCol]
    sig1 = sigInj[i]
    gap1 = sig1*np.sqrt(beta1*exn*pmass/nrj1)*1e3
    sig6 = sig150[i]
    gap6 = sig6*np.sqrt(beta6*exn*pmass/nrj)*1e3

    if( tct ):
        sig2 = gap6/(np.sqrt(beta2*exn*pmass/nrj)*1e3)
        sig3 = gap6/(np.sqrt(beta3*exn*pmass/nrj)*1e3)
        sig4 = gap6/(np.sqrt(beta4*exn*pmass/nrj)*1e3)
        sig5 = gap6/(np.sqrt(beta5*exn*pmass/nrj)*1e3)
        gap2 = gap6
        gap3 = gap6
        gap4 = gap6
        gap5 = gap6
    else:
        sig2 = sig6
        sig3 = sig6
        sig4 = sig6
        sig5 = sig6
        gap2 = sig2*np.sqrt(beta2*exn*pmass/nrj)*1e3
        gap3 = sig3*np.sqrt(beta3*exn*pmass/nrj)*1e3
        gap4 = sig4*np.sqrt(beta4*exn*pmass/nrj)*1e3
        gap5 = sig5*np.sqrt(beta5*exn*pmass/nrj)*1e3
    tempRow = [collim,b2Plane[i],
                beta1,beta2,beta3,beta4,beta5,beta6,
                sig1,sig2,sig3,sig4,sig5,sig6,
                gap1,gap2,gap3,gap4,gap5,gap6]       
    b2Res.append(tempRow) 
 

f = open('b1Res.csv','w')
writer = csv.writer(f)
[writer.writerow(row) for row in b1Res]
f.close()

f = open('b2Res.csv','w')
writer = csv.writer(f)
[writer.writerow(row) for row in b2Res]
f.close()                     
